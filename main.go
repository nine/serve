package main

import (
  "flag"
  "fmt"
  "log"
  "net"
  "net/http"
  "strings"
)

func main () {

  var dir string
  var port int
  var ping string

  flag.StringVar(&dir, "d", ".",
    "Specify directory")
  flag.IntVar(&port, "p", 3000,
    "Specify port")
  flag.StringVar(&ping, "ping", "8.8.8.8:80",
    "Specify ping target for fetching local IP")

  flag.Parse()

  var localIP string
  localIP = dialupLocalIP(ping)

  // Routes
  http.Handle("/",
    http.FileServer(http.Dir(dir)))

  // Server
  fmt.Printf("\nLaunching server on port %d\n\n", port)
  fmt.Printf("\thttp://localhost:%d\n", port)
  if localIP != "" {
    fmt.Printf("\thttp://%s:%d\n\n", localIP, port)
  }
  if localIP == "" {
    fmt.Printf("\nFailed to find local IP. Are you connected to a network?\n\n")
  }
  serverPortConfig := fmt.Sprintf(":%d", port)
  if err := http.ListenAndServe(serverPortConfig, nil); err != nil {
    log.Fatal(err)
  }

}

func dialupLocalIP (target string) string {
  conn, err := net.Dial("udp", target)
  if err != nil {
    return ""
  }
  defer conn.Close()

  addr := conn.LocalAddr().( *net.UDPAddr)
  ip := strings.Split(fmt.Sprint(addr), ":")
  return ip[0]
}
