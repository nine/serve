Small tool inspired by the [npm serve package](https://www.npmjs.com/package/serve). 

Clone and run `go build main.go` to build executable.

```
Args:
  -d string
    Specify directory (default ".")
  -p int
    Specify port (default 3000)
  -ping string
    Specify ping target for fetching local IP (default "8.8.8.8:80")
```
